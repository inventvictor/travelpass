﻿namespace TravelPass
{
    partial class RecordDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecordDashboard));
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.rfidCheck = new System.Windows.Forms.CheckBox();
            this.view_flight_details = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.flight_date_created_box = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.flight_date_box = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.flight_airline_box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.flight_from_box = new System.Windows.Forms.TextBox();
            this.flight_to_box = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.add_record = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.update_record = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.view_record = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.view_flights = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label6 = new System.Windows.Forms.Label();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.go_to_dashboard = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pers_id = new System.Windows.Forms.Label();
            this.pers_name = new System.Windows.Forms.Label();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuCards4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.bunifuCards3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.bunifuCards2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.bunifuCards1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.BackColor = System.Drawing.Color.White;
            this.bunifuCards4.BorderRadius = 0;
            this.bunifuCards4.BottomSahddow = true;
            this.bunifuCards4.color = System.Drawing.Color.White;
            this.bunifuCards4.Controls.Add(this.pictureBox5);
            this.bunifuCards4.Controls.Add(this.rfidCheck);
            this.bunifuCards4.Controls.Add(this.view_flight_details);
            this.bunifuCards4.Controls.Add(this.label4);
            this.bunifuCards4.Controls.Add(this.label8);
            this.bunifuCards4.Controls.Add(this.flight_date_created_box);
            this.bunifuCards4.Controls.Add(this.label7);
            this.bunifuCards4.Controls.Add(this.flight_date_box);
            this.bunifuCards4.Controls.Add(this.label5);
            this.bunifuCards4.Controls.Add(this.flight_airline_box);
            this.bunifuCards4.Controls.Add(this.label3);
            this.bunifuCards4.Controls.Add(this.flight_from_box);
            this.bunifuCards4.Controls.Add(this.flight_to_box);
            this.bunifuCards4.Controls.Add(this.pictureBox6);
            this.bunifuCards4.Controls.Add(this.pictureBox4);
            this.bunifuCards4.Controls.Add(this.bunifuCards3);
            this.bunifuCards4.Controls.Add(this.bunifuCards2);
            this.bunifuCards4.Controls.Add(this.bunifuCards1);
            this.bunifuCards4.Controls.Add(this.view_flights);
            this.bunifuCards4.Controls.Add(this.label6);
            this.bunifuCards4.Controls.Add(this.bunifuFlatButton1);
            this.bunifuCards4.Controls.Add(this.go_to_dashboard);
            this.bunifuCards4.Controls.Add(this.pers_id);
            this.bunifuCards4.Controls.Add(this.pers_name);
            this.bunifuCards4.Controls.Add(this.bunifuImageButton2);
            this.bunifuCards4.Controls.Add(this.label2);
            this.bunifuCards4.Controls.Add(this.label1);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(-1, 0);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = true;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(676, 528);
            this.bunifuCards4.TabIndex = 15;
            this.bunifuCards4.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuCards4_Paint);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::TravelPass.Properties.Resources.take_off_plane;
            this.pictureBox5.Location = new System.Drawing.Point(208, 76);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(64, 52);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            // 
            // rfidCheck
            // 
            this.rfidCheck.AutoSize = true;
            this.rfidCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.rfidCheck.Location = new System.Drawing.Point(18, 107);
            this.rfidCheck.Name = "rfidCheck";
            this.rfidCheck.Size = new System.Drawing.Size(193, 22);
            this.rfidCheck.TabIndex = 98;
            this.rfidCheck.Text = "Enable RFID for Records";
            this.rfidCheck.UseVisualStyleBackColor = true;
            this.rfidCheck.CheckedChanged += new System.EventHandler(this.rfidCheck_CheckedChanged);
            // 
            // view_flight_details
            // 
            this.view_flight_details.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(222)))));
            this.view_flight_details.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.view_flight_details.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.view_flight_details.ForeColor = System.Drawing.Color.White;
            this.view_flight_details.Location = new System.Drawing.Point(488, 10);
            this.view_flight_details.Name = "view_flight_details";
            this.view_flight_details.Size = new System.Drawing.Size(178, 43);
            this.view_flight_details.TabIndex = 97;
            this.view_flight_details.Text = "View Flight Details";
            this.view_flight_details.UseVisualStyleBackColor = false;
            this.view_flight_details.Click += new System.EventHandler(this.view_flight_details_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(507, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 17);
            this.label4.TabIndex = 24;
            this.label4.Text = "Flight To";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label8.Location = new System.Drawing.Point(465, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "Date Created";
            // 
            // flight_date_created_box
            // 
            this.flight_date_created_box.Location = new System.Drawing.Point(468, 159);
            this.flight_date_created_box.Name = "flight_date_created_box";
            this.flight_date_created_box.Size = new System.Drawing.Size(156, 20);
            this.flight_date_created_box.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label7.Location = new System.Drawing.Point(239, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 17);
            this.label7.TabIndex = 24;
            this.label7.Text = "Flight Date";
            // 
            // flight_date_box
            // 
            this.flight_date_box.Location = new System.Drawing.Point(242, 159);
            this.flight_date_box.Name = "flight_date_box";
            this.flight_date_box.Size = new System.Drawing.Size(156, 20);
            this.flight_date_box.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(15, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Flight Airline";
            // 
            // flight_airline_box
            // 
            this.flight_airline_box.Location = new System.Drawing.Point(18, 159);
            this.flight_airline_box.Name = "flight_airline_box";
            this.flight_airline_box.Size = new System.Drawing.Size(156, 20);
            this.flight_airline_box.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(275, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Flight From";
            // 
            // flight_from_box
            // 
            this.flight_from_box.Location = new System.Drawing.Point(278, 100);
            this.flight_from_box.Name = "flight_from_box";
            this.flight_from_box.Size = new System.Drawing.Size(156, 20);
            this.flight_from_box.TabIndex = 23;
            // 
            // flight_to_box
            // 
            this.flight_to_box.Location = new System.Drawing.Point(510, 100);
            this.flight_to_box.Name = "flight_to_box";
            this.flight_to_box.Size = new System.Drawing.Size(156, 20);
            this.flight_to_box.TabIndex = 23;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::TravelPass.Properties.Resources.land_plane;
            this.pictureBox6.Location = new System.Drawing.Point(440, 76);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(64, 52);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 22;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(-22, -46);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 50);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.White;
            this.bunifuCards3.BorderRadius = 30;
            this.bunifuCards3.BottomSahddow = true;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuCards3.Controls.Add(this.add_record);
            this.bunifuCards3.Controls.Add(this.pictureBox2);
            this.bunifuCards3.LeftSahddow = true;
            this.bunifuCards3.Location = new System.Drawing.Point(13, 213);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = true;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(199, 274);
            this.bunifuCards3.TabIndex = 19;
            this.bunifuCards3.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuCards3_Paint);
            // 
            // add_record
            // 
            this.add_record.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.add_record.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.add_record.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.add_record.BorderRadius = 8;
            this.add_record.ButtonText = "Add Record";
            this.add_record.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add_record.Iconcolor = System.Drawing.Color.Transparent;
            this.add_record.Iconimage = ((System.Drawing.Image)(resources.GetObject("add_record.Iconimage")));
            this.add_record.Iconimage_right = null;
            this.add_record.Iconimage_right_Selected = null;
            this.add_record.Iconimage_Selected = null;
            this.add_record.IconZoom = 90D;
            this.add_record.IsTab = false;
            this.add_record.Location = new System.Drawing.Point(23, 206);
            this.add_record.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.add_record.Name = "add_record";
            this.add_record.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.add_record.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.add_record.OnHoverTextColor = System.Drawing.Color.White;
            this.add_record.selected = false;
            this.add_record.Size = new System.Drawing.Size(148, 48);
            this.add_record.TabIndex = 12;
            this.add_record.Textcolor = System.Drawing.Color.White;
            this.add_record.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add_record.Click += new System.EventHandler(this.add_record_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::TravelPass.Properties.Resources.if_icon_105_folder_add_314682;
            this.pictureBox2.Location = new System.Drawing.Point(34, 48);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(127, 140);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.White;
            this.bunifuCards2.BorderRadius = 30;
            this.bunifuCards2.BottomSahddow = true;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuCards2.Controls.Add(this.pictureBox1);
            this.bunifuCards2.Controls.Add(this.update_record);
            this.bunifuCards2.LeftSahddow = true;
            this.bunifuCards2.Location = new System.Drawing.Point(242, 213);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = true;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(199, 274);
            this.bunifuCards2.TabIndex = 20;
            this.bunifuCards2.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuCards2_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::TravelPass.Properties.Resources.if_retweet_334739;
            this.pictureBox1.Location = new System.Drawing.Point(30, 66);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 110);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // update_record
            // 
            this.update_record.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.update_record.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.update_record.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.update_record.BorderRadius = 8;
            this.update_record.ButtonText = "Update Record";
            this.update_record.Cursor = System.Windows.Forms.Cursors.Hand;
            this.update_record.Iconcolor = System.Drawing.Color.Transparent;
            this.update_record.Iconimage = ((System.Drawing.Image)(resources.GetObject("update_record.Iconimage")));
            this.update_record.Iconimage_right = null;
            this.update_record.Iconimage_right_Selected = null;
            this.update_record.Iconimage_Selected = null;
            this.update_record.IconZoom = 90D;
            this.update_record.IsTab = false;
            this.update_record.Location = new System.Drawing.Point(18, 205);
            this.update_record.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.update_record.Name = "update_record";
            this.update_record.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.update_record.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.update_record.OnHoverTextColor = System.Drawing.Color.White;
            this.update_record.selected = false;
            this.update_record.Size = new System.Drawing.Size(156, 48);
            this.update_record.TabIndex = 12;
            this.update_record.Textcolor = System.Drawing.Color.White;
            this.update_record.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update_record.Click += new System.EventHandler(this.update_record_Click);
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 30;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuCards1.Controls.Add(this.view_record);
            this.bunifuCards1.Controls.Add(this.pictureBox3);
            this.bunifuCards1.LeftSahddow = true;
            this.bunifuCards1.Location = new System.Drawing.Point(468, 214);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(195, 273);
            this.bunifuCards1.TabIndex = 21;
            this.bunifuCards1.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuCards1_Paint);
            // 
            // view_record
            // 
            this.view_record.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.view_record.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.view_record.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view_record.BorderRadius = 8;
            this.view_record.ButtonText = "View Records";
            this.view_record.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_record.Iconcolor = System.Drawing.Color.Transparent;
            this.view_record.Iconimage = ((System.Drawing.Image)(resources.GetObject("view_record.Iconimage")));
            this.view_record.Iconimage_right = null;
            this.view_record.Iconimage_right_Selected = null;
            this.view_record.Iconimage_Selected = null;
            this.view_record.IconZoom = 90D;
            this.view_record.IsTab = false;
            this.view_record.Location = new System.Drawing.Point(23, 204);
            this.view_record.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.view_record.Name = "view_record";
            this.view_record.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.view_record.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.view_record.OnHoverTextColor = System.Drawing.Color.White;
            this.view_record.selected = false;
            this.view_record.Size = new System.Drawing.Size(148, 48);
            this.view_record.TabIndex = 22;
            this.view_record.Textcolor = System.Drawing.Color.White;
            this.view_record.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_record.Click += new System.EventHandler(this.view_record_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::TravelPass.Properties.Resources.if_search_254211;
            this.pictureBox3.Location = new System.Drawing.Point(31, 48);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(133, 138);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // view_flights
            // 
            this.view_flights.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.view_flights.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.view_flights.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view_flights.BorderRadius = 8;
            this.view_flights.ButtonText = "View Flights";
            this.view_flights.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_flights.Iconcolor = System.Drawing.Color.Transparent;
            this.view_flights.Iconimage = ((System.Drawing.Image)(resources.GetObject("view_flights.Iconimage")));
            this.view_flights.Iconimage_right = null;
            this.view_flights.Iconimage_right_Selected = null;
            this.view_flights.Iconimage_Selected = null;
            this.view_flights.IconZoom = 90D;
            this.view_flights.IsTab = false;
            this.view_flights.Location = new System.Drawing.Point(491, 362);
            this.view_flights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.view_flights.Name = "view_flights";
            this.view_flights.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.view_flights.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.view_flights.OnHoverTextColor = System.Drawing.Color.White;
            this.view_flights.selected = false;
            this.view_flights.Size = new System.Drawing.Size(148, 48);
            this.view_flights.TabIndex = 18;
            this.view_flights.Textcolor = System.Drawing.Color.White;
            this.view_flights.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(158, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(2, 50);
            this.label6.TabIndex = 17;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Go to Dashboard";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(-462, 155);
            this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.Red;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(164, 20);
            this.bunifuFlatButton1.TabIndex = 16;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.go_to_dashboard_Click);
            // 
            // go_to_dashboard
            // 
            this.go_to_dashboard.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.go_to_dashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.go_to_dashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.go_to_dashboard.BorderRadius = 0;
            this.go_to_dashboard.ButtonText = "Go to Dashboard";
            this.go_to_dashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.go_to_dashboard.Iconcolor = System.Drawing.Color.Transparent;
            this.go_to_dashboard.Iconimage = ((System.Drawing.Image)(resources.GetObject("go_to_dashboard.Iconimage")));
            this.go_to_dashboard.Iconimage_right = null;
            this.go_to_dashboard.Iconimage_right_Selected = null;
            this.go_to_dashboard.Iconimage_Selected = null;
            this.go_to_dashboard.IconZoom = 90D;
            this.go_to_dashboard.IsTab = false;
            this.go_to_dashboard.Location = new System.Drawing.Point(18, 64);
            this.go_to_dashboard.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.go_to_dashboard.Name = "go_to_dashboard";
            this.go_to_dashboard.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.go_to_dashboard.OnHovercolor = System.Drawing.Color.Red;
            this.go_to_dashboard.OnHoverTextColor = System.Drawing.Color.White;
            this.go_to_dashboard.selected = false;
            this.go_to_dashboard.Size = new System.Drawing.Size(171, 33);
            this.go_to_dashboard.TabIndex = 16;
            this.go_to_dashboard.Textcolor = System.Drawing.Color.White;
            this.go_to_dashboard.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.go_to_dashboard.Click += new System.EventHandler(this.go_to_dashboard_Click);
            // 
            // pers_id
            // 
            this.pers_id.AutoSize = true;
            this.pers_id.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pers_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pers_id.Location = new System.Drawing.Point(166, 31);
            this.pers_id.Name = "pers_id";
            this.pers_id.Size = new System.Drawing.Size(59, 19);
            this.pers_id.TabIndex = 10;
            this.pers_id.Text = "USR453";
            // 
            // pers_name
            // 
            this.pers_name.AutoSize = true;
            this.pers_name.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pers_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pers_name.Location = new System.Drawing.Point(166, 8);
            this.pers_name.Name = "pers_name";
            this.pers_name.Size = new System.Drawing.Size(153, 23);
            this.pers_name.TabIndex = 10;
            this.pers_name.Text = "CHIZOBA ARINZE";
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.BackColor = System.Drawing.Color.White;
            this.bunifuImageButton2.Image = global::TravelPass.Properties.Resources.newpng;
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(13, 8);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(54, 49);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 14;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(73, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Records";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(73, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "ALML";
            // 
            // RecordDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 517);
            this.Controls.Add(this.bunifuCards4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RecordDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.RecordDashboard_Load);
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.bunifuCards3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.bunifuCards2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.bunifuCards1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuFlatButton go_to_dashboard;
        private System.Windows.Forms.Label pers_id;
        private System.Windows.Forms.Label pers_name;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private Bunifu.Framework.UI.BunifuFlatButton add_record;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuFlatButton update_record;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Bunifu.Framework.UI.BunifuFlatButton view_flights;
        private Bunifu.Framework.UI.BunifuFlatButton view_record;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox flight_from_box;
        private System.Windows.Forms.TextBox flight_to_box;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox flight_date_created_box;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox flight_date_box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox flight_airline_box;
        private System.Windows.Forms.Button view_flight_details;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.CheckBox rfidCheck;
    }
}