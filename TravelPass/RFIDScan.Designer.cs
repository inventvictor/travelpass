﻿namespace TravelPass
{
    partial class RFIDScan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.codelineRichTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.dg_12 = new System.Windows.Forms.Label();
            this.dg12 = new System.Windows.Forms.Label();
            this.dg_11 = new System.Windows.Forms.Label();
            this.dg11 = new System.Windows.Forms.Label();
            this.dg_10 = new System.Windows.Forms.Label();
            this.dg10 = new System.Windows.Forms.Label();
            this.dg_9 = new System.Windows.Forms.Label();
            this.dg9 = new System.Windows.Forms.Label();
            this.dg_8 = new System.Windows.Forms.Label();
            this.dg8 = new System.Windows.Forms.Label();
            this.dg_16 = new System.Windows.Forms.Label();
            this.dg16 = new System.Windows.Forms.Label();
            this.dg_15 = new System.Windows.Forms.Label();
            this.dg15 = new System.Windows.Forms.Label();
            this.dg_14 = new System.Windows.Forms.Label();
            this.dg_13 = new System.Windows.Forms.Label();
            this.dg14 = new System.Windows.Forms.Label();
            this.dg_7 = new System.Windows.Forms.Label();
            this.dg13 = new System.Windows.Forms.Label();
            this.dg_6 = new System.Windows.Forms.Label();
            this.dg7 = new System.Windows.Forms.Label();
            this.dg_5 = new System.Windows.Forms.Label();
            this.dg6 = new System.Windows.Forms.Label();
            this.dg_4 = new System.Windows.Forms.Label();
            this.dg5 = new System.Windows.Forms.Label();
            this.dg_3 = new System.Windows.Forms.Label();
            this.dg4 = new System.Windows.Forms.Label();
            this.dg_2 = new System.Windows.Forms.Label();
            this.dg3 = new System.Windows.Forms.Label();
            this.dg_1 = new System.Windows.Forms.Label();
            this.dg2 = new System.Windows.Forms.Label();
            this.dg1 = new System.Windows.Forms.Label();
            this.rfidREF = new System.Windows.Forms.Button();
            this.rfZoom = new System.Windows.Forms.Button();
            this.chipID = new System.Windows.Forms.Label();
            this.caImage = new System.Windows.Forms.PictureBox();
            this.paImage = new System.Windows.Forms.PictureBox();
            this.taImage = new System.Windows.Forms.PictureBox();
            this.aaImage = new System.Windows.Forms.PictureBox();
            this.docsImage = new System.Windows.Forms.PictureBox();
            this.siImage = new System.Windows.Forms.PictureBox();
            this.saImage = new System.Windows.Forms.PictureBox();
            this.rfImage = new System.Windows.Forms.PictureBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.caImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docsImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfImage)).BeginInit();
            this.SuspendLayout();
            // 
            // codelineRichTextBox
            // 
            this.codelineRichTextBox.BackColor = System.Drawing.Color.White;
            this.codelineRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.codelineRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.codelineRichTextBox.Location = new System.Drawing.Point(4, 350);
            this.codelineRichTextBox.Name = "codelineRichTextBox";
            this.codelineRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.codelineRichTextBox.Size = new System.Drawing.Size(395, 80);
            this.codelineRichTextBox.TabIndex = 83;
            this.codelineRichTextBox.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.caImage);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.paImage);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.taImage);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.aaImage);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.docsImage);
            this.groupBox2.Controls.Add(this.siImage);
            this.groupBox2.Controls.Add(this.saImage);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(177, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(402, 112);
            this.groupBox2.TabIndex = 82;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Validation";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(318, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 15);
            this.label16.TabIndex = 1;
            this.label16.Text = "Chip Auth";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(196, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 15);
            this.label15.TabIndex = 1;
            this.label15.Text = "Passive Auth";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(318, 55);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 15);
            this.label19.TabIndex = 1;
            this.label19.Text = "Term Auth";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(196, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 15);
            this.label18.TabIndex = 1;
            this.label18.Text = "Active Auth";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(39, 82);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(108, 15);
            this.label20.TabIndex = 1;
            this.label20.Text = "Doc Signer Cert";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(39, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 15);
            this.label17.TabIndex = 1;
            this.label17.Text = "Signature";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(39, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 15);
            this.label10.TabIndex = 1;
            this.label10.Text = "Signed Attributes";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label61);
            this.groupBox1.Controls.Add(this.label60);
            this.groupBox1.Controls.Add(this.label59);
            this.groupBox1.Controls.Add(this.dg_12);
            this.groupBox1.Controls.Add(this.dg12);
            this.groupBox1.Controls.Add(this.dg_11);
            this.groupBox1.Controls.Add(this.dg11);
            this.groupBox1.Controls.Add(this.dg_10);
            this.groupBox1.Controls.Add(this.dg10);
            this.groupBox1.Controls.Add(this.dg_9);
            this.groupBox1.Controls.Add(this.dg9);
            this.groupBox1.Controls.Add(this.dg_8);
            this.groupBox1.Controls.Add(this.dg8);
            this.groupBox1.Controls.Add(this.dg_16);
            this.groupBox1.Controls.Add(this.dg16);
            this.groupBox1.Controls.Add(this.dg_15);
            this.groupBox1.Controls.Add(this.dg15);
            this.groupBox1.Controls.Add(this.dg_14);
            this.groupBox1.Controls.Add(this.dg_13);
            this.groupBox1.Controls.Add(this.dg14);
            this.groupBox1.Controls.Add(this.dg_7);
            this.groupBox1.Controls.Add(this.dg13);
            this.groupBox1.Controls.Add(this.dg_6);
            this.groupBox1.Controls.Add(this.dg7);
            this.groupBox1.Controls.Add(this.dg_5);
            this.groupBox1.Controls.Add(this.dg6);
            this.groupBox1.Controls.Add(this.dg_4);
            this.groupBox1.Controls.Add(this.dg5);
            this.groupBox1.Controls.Add(this.dg_3);
            this.groupBox1.Controls.Add(this.dg4);
            this.groupBox1.Controls.Add(this.dg_2);
            this.groupBox1.Controls.Add(this.dg3);
            this.groupBox1.Controls.Add(this.dg_1);
            this.groupBox1.Controls.Add(this.dg2);
            this.groupBox1.Controls.Add(this.dg1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(177, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 146);
            this.groupBox1.TabIndex = 81;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Groups";
            // 
            // label61
            // 
            this.label61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label61.Location = new System.Drawing.Point(155, 8);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(2, 150);
            this.label61.TabIndex = 46;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(192, 21);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(67, 15);
            this.label60.TabIndex = 1;
            this.label60.Text = "Validated";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(45, 22);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 15);
            this.label59.TabIndex = 1;
            this.label59.Text = "Read";
            // 
            // dg_12
            // 
            this.dg_12.AutoSize = true;
            this.dg_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_12.ForeColor = System.Drawing.Color.White;
            this.dg_12.Location = new System.Drawing.Point(203, 121);
            this.dg_12.Name = "dg_12";
            this.dg_12.Size = new System.Drawing.Size(45, 17);
            this.dg_12.TabIndex = 0;
            this.dg_12.Text = "DG12";
            // 
            // dg12
            // 
            this.dg12.AutoSize = true;
            this.dg12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg12.ForeColor = System.Drawing.Color.White;
            this.dg12.Location = new System.Drawing.Point(48, 122);
            this.dg12.Name = "dg12";
            this.dg12.Size = new System.Drawing.Size(45, 17);
            this.dg12.TabIndex = 0;
            this.dg12.Text = "DG12";
            // 
            // dg_11
            // 
            this.dg_11.AutoSize = true;
            this.dg_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_11.ForeColor = System.Drawing.Color.White;
            this.dg_11.Location = new System.Drawing.Point(203, 104);
            this.dg_11.Name = "dg_11";
            this.dg_11.Size = new System.Drawing.Size(45, 17);
            this.dg_11.TabIndex = 0;
            this.dg_11.Text = "DG11";
            // 
            // dg11
            // 
            this.dg11.AutoSize = true;
            this.dg11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg11.ForeColor = System.Drawing.Color.White;
            this.dg11.Location = new System.Drawing.Point(48, 105);
            this.dg11.Name = "dg11";
            this.dg11.Size = new System.Drawing.Size(45, 17);
            this.dg11.TabIndex = 0;
            this.dg11.Text = "DG11";
            // 
            // dg_10
            // 
            this.dg_10.AutoSize = true;
            this.dg_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_10.ForeColor = System.Drawing.Color.White;
            this.dg_10.Location = new System.Drawing.Point(203, 87);
            this.dg_10.Name = "dg_10";
            this.dg_10.Size = new System.Drawing.Size(45, 17);
            this.dg_10.TabIndex = 0;
            this.dg_10.Text = "DG10";
            // 
            // dg10
            // 
            this.dg10.AutoSize = true;
            this.dg10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg10.ForeColor = System.Drawing.Color.White;
            this.dg10.Location = new System.Drawing.Point(48, 88);
            this.dg10.Name = "dg10";
            this.dg10.Size = new System.Drawing.Size(45, 17);
            this.dg10.TabIndex = 0;
            this.dg10.Text = "DG10";
            // 
            // dg_9
            // 
            this.dg_9.AutoSize = true;
            this.dg_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_9.ForeColor = System.Drawing.Color.White;
            this.dg_9.Location = new System.Drawing.Point(203, 71);
            this.dg_9.Name = "dg_9";
            this.dg_9.Size = new System.Drawing.Size(37, 17);
            this.dg_9.TabIndex = 0;
            this.dg_9.Text = "DG9";
            // 
            // dg9
            // 
            this.dg9.AutoSize = true;
            this.dg9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg9.ForeColor = System.Drawing.Color.White;
            this.dg9.Location = new System.Drawing.Point(48, 72);
            this.dg9.Name = "dg9";
            this.dg9.Size = new System.Drawing.Size(37, 17);
            this.dg9.TabIndex = 0;
            this.dg9.Text = "DG9";
            // 
            // dg_8
            // 
            this.dg_8.AutoSize = true;
            this.dg_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_8.ForeColor = System.Drawing.Color.White;
            this.dg_8.Location = new System.Drawing.Point(203, 55);
            this.dg_8.Name = "dg_8";
            this.dg_8.Size = new System.Drawing.Size(37, 17);
            this.dg_8.TabIndex = 0;
            this.dg_8.Text = "DG8";
            // 
            // dg8
            // 
            this.dg8.AutoSize = true;
            this.dg8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg8.ForeColor = System.Drawing.Color.White;
            this.dg8.Location = new System.Drawing.Point(48, 56);
            this.dg8.Name = "dg8";
            this.dg8.Size = new System.Drawing.Size(37, 17);
            this.dg8.TabIndex = 0;
            this.dg8.Text = "DG8";
            // 
            // dg_16
            // 
            this.dg_16.AutoSize = true;
            this.dg_16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_16.ForeColor = System.Drawing.Color.White;
            this.dg_16.Location = new System.Drawing.Point(246, 87);
            this.dg_16.Name = "dg_16";
            this.dg_16.Size = new System.Drawing.Size(45, 17);
            this.dg_16.TabIndex = 0;
            this.dg_16.Text = "DG16";
            // 
            // dg16
            // 
            this.dg16.AutoSize = true;
            this.dg16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg16.ForeColor = System.Drawing.Color.White;
            this.dg16.Location = new System.Drawing.Point(91, 88);
            this.dg16.Name = "dg16";
            this.dg16.Size = new System.Drawing.Size(45, 17);
            this.dg16.TabIndex = 0;
            this.dg16.Text = "DG16";
            // 
            // dg_15
            // 
            this.dg_15.AutoSize = true;
            this.dg_15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_15.ForeColor = System.Drawing.Color.White;
            this.dg_15.Location = new System.Drawing.Point(240, 70);
            this.dg_15.Name = "dg_15";
            this.dg_15.Size = new System.Drawing.Size(45, 17);
            this.dg_15.TabIndex = 0;
            this.dg_15.Text = "DG15";
            // 
            // dg15
            // 
            this.dg15.AutoSize = true;
            this.dg15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg15.ForeColor = System.Drawing.Color.White;
            this.dg15.Location = new System.Drawing.Point(85, 71);
            this.dg15.Name = "dg15";
            this.dg15.Size = new System.Drawing.Size(45, 17);
            this.dg15.TabIndex = 0;
            this.dg15.Text = "DG15";
            // 
            // dg_14
            // 
            this.dg_14.AutoSize = true;
            this.dg_14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_14.ForeColor = System.Drawing.Color.White;
            this.dg_14.Location = new System.Drawing.Point(240, 54);
            this.dg_14.Name = "dg_14";
            this.dg_14.Size = new System.Drawing.Size(45, 17);
            this.dg_14.TabIndex = 0;
            this.dg_14.Text = "DG14";
            // 
            // dg_13
            // 
            this.dg_13.AutoSize = true;
            this.dg_13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_13.ForeColor = System.Drawing.Color.White;
            this.dg_13.Location = new System.Drawing.Point(240, 38);
            this.dg_13.Name = "dg_13";
            this.dg_13.Size = new System.Drawing.Size(45, 17);
            this.dg_13.TabIndex = 0;
            this.dg_13.Text = "DG13";
            // 
            // dg14
            // 
            this.dg14.AutoSize = true;
            this.dg14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg14.ForeColor = System.Drawing.Color.White;
            this.dg14.Location = new System.Drawing.Point(85, 55);
            this.dg14.Name = "dg14";
            this.dg14.Size = new System.Drawing.Size(45, 17);
            this.dg14.TabIndex = 0;
            this.dg14.Text = "DG14";
            // 
            // dg_7
            // 
            this.dg_7.AutoSize = true;
            this.dg_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_7.ForeColor = System.Drawing.Color.White;
            this.dg_7.Location = new System.Drawing.Point(203, 38);
            this.dg_7.Name = "dg_7";
            this.dg_7.Size = new System.Drawing.Size(37, 17);
            this.dg_7.TabIndex = 0;
            this.dg_7.Text = "DG7";
            // 
            // dg13
            // 
            this.dg13.AutoSize = true;
            this.dg13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg13.ForeColor = System.Drawing.Color.White;
            this.dg13.Location = new System.Drawing.Point(85, 39);
            this.dg13.Name = "dg13";
            this.dg13.Size = new System.Drawing.Size(45, 17);
            this.dg13.TabIndex = 0;
            this.dg13.Text = "DG13";
            // 
            // dg_6
            // 
            this.dg_6.AutoSize = true;
            this.dg_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_6.ForeColor = System.Drawing.Color.White;
            this.dg_6.Location = new System.Drawing.Point(166, 121);
            this.dg_6.Name = "dg_6";
            this.dg_6.Size = new System.Drawing.Size(37, 17);
            this.dg_6.TabIndex = 0;
            this.dg_6.Text = "DG6";
            // 
            // dg7
            // 
            this.dg7.AutoSize = true;
            this.dg7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg7.ForeColor = System.Drawing.Color.White;
            this.dg7.Location = new System.Drawing.Point(48, 39);
            this.dg7.Name = "dg7";
            this.dg7.Size = new System.Drawing.Size(37, 17);
            this.dg7.TabIndex = 0;
            this.dg7.Text = "DG7";
            // 
            // dg_5
            // 
            this.dg_5.AutoSize = true;
            this.dg_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_5.ForeColor = System.Drawing.Color.White;
            this.dg_5.Location = new System.Drawing.Point(166, 104);
            this.dg_5.Name = "dg_5";
            this.dg_5.Size = new System.Drawing.Size(37, 17);
            this.dg_5.TabIndex = 0;
            this.dg_5.Text = "DG5";
            // 
            // dg6
            // 
            this.dg6.AutoSize = true;
            this.dg6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg6.ForeColor = System.Drawing.Color.White;
            this.dg6.Location = new System.Drawing.Point(11, 122);
            this.dg6.Name = "dg6";
            this.dg6.Size = new System.Drawing.Size(37, 17);
            this.dg6.TabIndex = 0;
            this.dg6.Text = "DG6";
            // 
            // dg_4
            // 
            this.dg_4.AutoSize = true;
            this.dg_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_4.ForeColor = System.Drawing.Color.White;
            this.dg_4.Location = new System.Drawing.Point(166, 89);
            this.dg_4.Name = "dg_4";
            this.dg_4.Size = new System.Drawing.Size(37, 17);
            this.dg_4.TabIndex = 0;
            this.dg_4.Text = "DG4";
            // 
            // dg5
            // 
            this.dg5.AutoSize = true;
            this.dg5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg5.ForeColor = System.Drawing.Color.White;
            this.dg5.Location = new System.Drawing.Point(11, 105);
            this.dg5.Name = "dg5";
            this.dg5.Size = new System.Drawing.Size(37, 17);
            this.dg5.TabIndex = 0;
            this.dg5.Text = "DG5";
            // 
            // dg_3
            // 
            this.dg_3.AutoSize = true;
            this.dg_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_3.ForeColor = System.Drawing.Color.White;
            this.dg_3.Location = new System.Drawing.Point(166, 72);
            this.dg_3.Name = "dg_3";
            this.dg_3.Size = new System.Drawing.Size(37, 17);
            this.dg_3.TabIndex = 0;
            this.dg_3.Text = "DG3";
            // 
            // dg4
            // 
            this.dg4.AutoSize = true;
            this.dg4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg4.ForeColor = System.Drawing.Color.White;
            this.dg4.Location = new System.Drawing.Point(11, 90);
            this.dg4.Name = "dg4";
            this.dg4.Size = new System.Drawing.Size(37, 17);
            this.dg4.TabIndex = 0;
            this.dg4.Text = "DG4";
            // 
            // dg_2
            // 
            this.dg_2.AutoSize = true;
            this.dg_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_2.ForeColor = System.Drawing.Color.White;
            this.dg_2.Location = new System.Drawing.Point(166, 55);
            this.dg_2.Name = "dg_2";
            this.dg_2.Size = new System.Drawing.Size(37, 17);
            this.dg_2.TabIndex = 0;
            this.dg_2.Text = "DG2";
            // 
            // dg3
            // 
            this.dg3.AutoSize = true;
            this.dg3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg3.ForeColor = System.Drawing.Color.White;
            this.dg3.Location = new System.Drawing.Point(11, 73);
            this.dg3.Name = "dg3";
            this.dg3.Size = new System.Drawing.Size(37, 17);
            this.dg3.TabIndex = 0;
            this.dg3.Text = "DG3";
            // 
            // dg_1
            // 
            this.dg_1.AutoSize = true;
            this.dg_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg_1.ForeColor = System.Drawing.Color.White;
            this.dg_1.Location = new System.Drawing.Point(166, 38);
            this.dg_1.Name = "dg_1";
            this.dg_1.Size = new System.Drawing.Size(37, 17);
            this.dg_1.TabIndex = 0;
            this.dg_1.Text = "DG1";
            // 
            // dg2
            // 
            this.dg2.AutoSize = true;
            this.dg2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg2.ForeColor = System.Drawing.Color.White;
            this.dg2.Location = new System.Drawing.Point(11, 56);
            this.dg2.Name = "dg2";
            this.dg2.Size = new System.Drawing.Size(37, 17);
            this.dg2.TabIndex = 0;
            this.dg2.Text = "DG2";
            // 
            // dg1
            // 
            this.dg1.AutoSize = true;
            this.dg1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dg1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dg1.ForeColor = System.Drawing.Color.White;
            this.dg1.Location = new System.Drawing.Point(11, 39);
            this.dg1.Name = "dg1";
            this.dg1.Size = new System.Drawing.Size(37, 17);
            this.dg1.TabIndex = 0;
            this.dg1.Text = "DG1";
            // 
            // rfidREF
            // 
            this.rfidREF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rfidREF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rfidREF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rfidREF.ForeColor = System.Drawing.Color.White;
            this.rfidREF.Location = new System.Drawing.Point(414, 361);
            this.rfidREF.Name = "rfidREF";
            this.rfidREF.Size = new System.Drawing.Size(178, 48);
            this.rfidREF.TabIndex = 96;
            this.rfidREF.Text = "ePassport Data Group Reference";
            this.rfidREF.UseVisualStyleBackColor = false;
            this.rfidREF.Click += new System.EventHandler(this.rfidREF_Click);
            // 
            // rfZoom
            // 
            this.rfZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rfZoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rfZoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rfZoom.ForeColor = System.Drawing.Color.White;
            this.rfZoom.Location = new System.Drawing.Point(4, 210);
            this.rfZoom.Name = "rfZoom";
            this.rfZoom.Size = new System.Drawing.Size(156, 43);
            this.rfZoom.TabIndex = 96;
            this.rfZoom.Text = "View Zoomed";
            this.rfZoom.UseVisualStyleBackColor = false;
            this.rfZoom.Click += new System.EventHandler(this.rfZoom_Click);
            // 
            // chipID
            // 
            this.chipID.AutoSize = true;
            this.chipID.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chipID.Location = new System.Drawing.Point(3, 10);
            this.chipID.Name = "chipID";
            this.chipID.Size = new System.Drawing.Size(0, 31);
            this.chipID.TabIndex = 97;
            // 
            // caImage
            // 
            this.caImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.caImage.Location = new System.Drawing.Point(290, 24);
            this.caImage.Name = "caImage";
            this.caImage.Size = new System.Drawing.Size(25, 21);
            this.caImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.caImage.TabIndex = 0;
            this.caImage.TabStop = false;
            // 
            // paImage
            // 
            this.paImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.paImage.Location = new System.Drawing.Point(168, 24);
            this.paImage.Name = "paImage";
            this.paImage.Size = new System.Drawing.Size(25, 21);
            this.paImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.paImage.TabIndex = 0;
            this.paImage.TabStop = false;
            // 
            // taImage
            // 
            this.taImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.taImage.Location = new System.Drawing.Point(290, 51);
            this.taImage.Name = "taImage";
            this.taImage.Size = new System.Drawing.Size(25, 21);
            this.taImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.taImage.TabIndex = 0;
            this.taImage.TabStop = false;
            // 
            // aaImage
            // 
            this.aaImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.aaImage.Location = new System.Drawing.Point(168, 51);
            this.aaImage.Name = "aaImage";
            this.aaImage.Size = new System.Drawing.Size(25, 21);
            this.aaImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.aaImage.TabIndex = 0;
            this.aaImage.TabStop = false;
            // 
            // docsImage
            // 
            this.docsImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.docsImage.Location = new System.Drawing.Point(11, 78);
            this.docsImage.Name = "docsImage";
            this.docsImage.Size = new System.Drawing.Size(25, 21);
            this.docsImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.docsImage.TabIndex = 0;
            this.docsImage.TabStop = false;
            // 
            // siImage
            // 
            this.siImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.siImage.Location = new System.Drawing.Point(11, 51);
            this.siImage.Name = "siImage";
            this.siImage.Size = new System.Drawing.Size(25, 21);
            this.siImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.siImage.TabIndex = 0;
            this.siImage.TabStop = false;
            // 
            // saImage
            // 
            this.saImage.Image = global::TravelPass.Properties.Resources.if_file_blank_paper_page_document_3209256;
            this.saImage.Location = new System.Drawing.Point(11, 24);
            this.saImage.Name = "saImage";
            this.saImage.Size = new System.Drawing.Size(25, 21);
            this.saImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.saImage.TabIndex = 0;
            this.saImage.TabStop = false;
            // 
            // rfImage
            // 
            this.rfImage.Location = new System.Drawing.Point(4, 52);
            this.rfImage.Name = "rfImage";
            this.rfImage.Size = new System.Drawing.Size(156, 158);
            this.rfImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.rfImage.TabIndex = 80;
            this.rfImage.TabStop = false;
            // 
            // RFIDScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.chipID);
            this.Controls.Add(this.rfZoom);
            this.Controls.Add(this.rfidREF);
            this.Controls.Add(this.codelineRichTextBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rfImage);
            this.Name = "RFIDScan";
            this.Size = new System.Drawing.Size(618, 433);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.caImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aaImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docsImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RichTextBox codelineRichTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.PictureBox caImage;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.PictureBox paImage;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.PictureBox taImage;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.PictureBox aaImage;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.PictureBox docsImage;
        public System.Windows.Forms.PictureBox siImage;
        public System.Windows.Forms.PictureBox saImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        public System.Windows.Forms.Label dg_12;
        public System.Windows.Forms.Label dg12;
        public System.Windows.Forms.Label dg_11;
        public System.Windows.Forms.Label dg11;
        public System.Windows.Forms.Label dg_10;
        public System.Windows.Forms.Label dg10;
        public System.Windows.Forms.Label dg_9;
        public System.Windows.Forms.Label dg9;
        public System.Windows.Forms.Label dg_8;
        public System.Windows.Forms.Label dg8;
        public System.Windows.Forms.Label dg_16;
        public System.Windows.Forms.Label dg16;
        public System.Windows.Forms.Label dg_15;
        public System.Windows.Forms.Label dg15;
        public System.Windows.Forms.Label dg_14;
        public System.Windows.Forms.Label dg_13;
        public System.Windows.Forms.Label dg14;
        public System.Windows.Forms.Label dg_7;
        public System.Windows.Forms.Label dg13;
        public System.Windows.Forms.Label dg_6;
        public System.Windows.Forms.Label dg7;
        public System.Windows.Forms.Label dg_5;
        public System.Windows.Forms.Label dg6;
        public System.Windows.Forms.Label dg_4;
        public System.Windows.Forms.Label dg5;
        public System.Windows.Forms.Label dg_3;
        public System.Windows.Forms.Label dg4;
        public System.Windows.Forms.Label dg_2;
        public System.Windows.Forms.Label dg3;
        public System.Windows.Forms.Label dg_1;
        public System.Windows.Forms.Label dg2;
        public System.Windows.Forms.Label dg1;
        public System.Windows.Forms.PictureBox rfImage;
        private System.Windows.Forms.Button rfidREF;
        private System.Windows.Forms.Button rfZoom;
        public System.Windows.Forms.Label chipID;
    }
}
