﻿namespace TravelPass
{
    partial class CreateAnotherPersonnel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.logo__setup = new System.Windows.Forms.PictureBox();
            this.topPanelSetup = new System.Windows.Forms.Panel();
            this.sign_in_travelpass = new System.Windows.Forms.Button();
            this.create_personnel_button = new System.Windows.Forms.Button();
            this.quitSetupLabel = new System.Windows.Forms.Label();
            this.minimizeSetupLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.minimizeSetup = new System.Windows.Forms.PictureBox();
            this.quitSetup = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo__setup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quitSetup)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "TravelPass";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.panel1.Controls.Add(this.logo__setup);
            this.panel1.Controls.Add(this.topPanelSetup);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 408);
            this.panel1.TabIndex = 1;
            // 
            // logo__setup
            // 
            this.logo__setup.Image = global::TravelPass.Properties.Resources.TravelPass_Logo_Blue_Icon_Round_2_8_18;
            this.logo__setup.Location = new System.Drawing.Point(49, 149);
            this.logo__setup.Name = "logo__setup";
            this.logo__setup.Size = new System.Drawing.Size(100, 101);
            this.logo__setup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo__setup.TabIndex = 9;
            this.logo__setup.TabStop = false;
            // 
            // topPanelSetup
            // 
            this.topPanelSetup.Location = new System.Drawing.Point(199, 0);
            this.topPanelSetup.Name = "topPanelSetup";
            this.topPanelSetup.Size = new System.Drawing.Size(294, 53);
            this.topPanelSetup.TabIndex = 7;
            // 
            // sign_in_travelpass
            // 
            this.sign_in_travelpass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sign_in_travelpass.Font = new System.Drawing.Font("Microsoft JhengHei UI Light", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sign_in_travelpass.ForeColor = System.Drawing.Color.White;
            this.sign_in_travelpass.Location = new System.Drawing.Point(209, 216);
            this.sign_in_travelpass.Name = "sign_in_travelpass";
            this.sign_in_travelpass.Size = new System.Drawing.Size(273, 34);
            this.sign_in_travelpass.TabIndex = 4;
            this.sign_in_travelpass.Text = " Sign intoTravelPass";
            this.sign_in_travelpass.UseVisualStyleBackColor = false;
            this.sign_in_travelpass.Click += new System.EventHandler(this.signIn_Click);
            // 
            // create_personnel_button
            // 
            this.create_personnel_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.create_personnel_button.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.create_personnel_button.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.create_personnel_button.Location = new System.Drawing.Point(209, 149);
            this.create_personnel_button.Name = "create_personnel_button";
            this.create_personnel_button.Size = new System.Drawing.Size(273, 34);
            this.create_personnel_button.TabIndex = 4;
            this.create_personnel_button.Text = "Create Another Personnel";
            this.create_personnel_button.UseVisualStyleBackColor = false;
            this.create_personnel_button.Click += new System.EventHandler(this.create_personnel_button_Click);
            // 
            // quitSetupLabel
            // 
            this.quitSetupLabel.AutoSize = true;
            this.quitSetupLabel.Location = new System.Drawing.Point(462, 30);
            this.quitSetupLabel.Name = "quitSetupLabel";
            this.quitSetupLabel.Size = new System.Drawing.Size(38, 20);
            this.quitSetupLabel.TabIndex = 6;
            this.quitSetupLabel.Text = "Quit";
            this.quitSetupLabel.Visible = false;
            // 
            // minimizeSetupLabel
            // 
            this.minimizeSetupLabel.AutoSize = true;
            this.minimizeSetupLabel.Location = new System.Drawing.Point(405, 30);
            this.minimizeSetupLabel.Name = "minimizeSetupLabel";
            this.minimizeSetupLabel.Size = new System.Drawing.Size(70, 20);
            this.minimizeSetupLabel.TabIndex = 6;
            this.minimizeSetupLabel.Text = "Minimize";
            this.minimizeSetupLabel.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(206, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(193, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Create Another Personnel";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // minimizeSetup
            // 
            this.minimizeSetup.Image = global::TravelPass.Properties.Resources.Minimize_Window_96px;
            this.minimizeSetup.Location = new System.Drawing.Point(431, 0);
            this.minimizeSetup.Name = "minimizeSetup";
            this.minimizeSetup.Size = new System.Drawing.Size(28, 27);
            this.minimizeSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.minimizeSetup.TabIndex = 5;
            this.minimizeSetup.TabStop = false;
            // 
            // quitSetup
            // 
            this.quitSetup.Image = global::TravelPass.Properties.Resources.Delete_96px;
            this.quitSetup.Location = new System.Drawing.Point(465, 0);
            this.quitSetup.Name = "quitSetup";
            this.quitSetup.Size = new System.Drawing.Size(28, 27);
            this.quitSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.quitSetup.TabIndex = 5;
            this.quitSetup.TabStop = false;
            this.quitSetup.Click += new System.EventHandler(this.quitSetup_Click);
            // 
            // CreateAnotherPersonnel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(494, 408);
            this.ControlBox = false;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.minimizeSetupLabel);
            this.Controls.Add(this.quitSetupLabel);
            this.Controls.Add(this.minimizeSetup);
            this.Controls.Add(this.quitSetup);
            this.Controls.Add(this.create_personnel_button);
            this.Controls.Add(this.sign_in_travelpass);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateAnotherPersonnel";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TravelPass";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logo__setup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quitSetup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button sign_in_travelpass;
        private System.Windows.Forms.Button create_personnel_button;
        private System.Windows.Forms.PictureBox quitSetup;
        private System.Windows.Forms.PictureBox minimizeSetup;
        private System.Windows.Forms.Label quitSetupLabel;
        private System.Windows.Forms.Label minimizeSetupLabel;
        private System.Windows.Forms.Panel topPanelSetup;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox logo__setup;

    }
}

