﻿namespace TravelPass
{
    partial class MRZScan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.issuer = new System.Windows.Forms.TextBox();
            this.doe = new System.Windows.Forms.TextBox();
            this.dob = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nationality = new System.Windows.Forms.TextBox();
            this.sex = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.given_names = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.doc_no = new System.Windows.Forms.TextBox();
            this.family_name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBoxCodeline = new System.Windows.Forms.RichTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.showVerification = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dob_flag = new System.Windows.Forms.Label();
            this.doe_flag = new System.Windows.Forms.Label();
            this.vd_flag = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.checks_flag = new System.Windows.Forms.Label();
            this.dn = new System.Windows.Forms.Label();
            this.doc_no_flag = new System.Windows.Forms.Label();
            this.expired_txt = new System.Windows.Forms.Label();
            this.mrzZoom = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.flagged_flag = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.oid_flag = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cdm_flag = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.rfid_flag = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.opt_data = new System.Windows.Forms.TextBox();
            this.mrzImage = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.age = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.mrzImage)).BeginInit();
            this.SuspendLayout();
            // 
            // issuer
            // 
            this.issuer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.issuer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.issuer.Location = new System.Drawing.Point(412, 460);
            this.issuer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.issuer.Name = "issuer";
            this.issuer.Size = new System.Drawing.Size(185, 28);
            this.issuer.TabIndex = 85;
            // 
            // doe
            // 
            this.doe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.doe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.doe.Location = new System.Drawing.Point(202, 460);
            this.doe.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.doe.Name = "doe";
            this.doe.Size = new System.Drawing.Size(168, 28);
            this.doe.TabIndex = 86;
            // 
            // dob
            // 
            this.dob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dob.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.dob.Location = new System.Drawing.Point(202, 377);
            this.dob.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dob.Name = "dob";
            this.dob.Size = new System.Drawing.Size(168, 28);
            this.dob.TabIndex = 87;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(408, 432);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 22);
            this.label8.TabIndex = 80;
            this.label8.Text = "&Issuer";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(198, 432);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 22);
            this.label9.TabIndex = 84;
            this.label9.Text = "&Date of Expiry";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(198, 349);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 22);
            this.label6.TabIndex = 83;
            this.label6.Text = "&Date of Birth";
            // 
            // nationality
            // 
            this.nationality.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.nationality.Location = new System.Drawing.Point(412, 294);
            this.nationality.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nationality.Name = "nationality";
            this.nationality.Size = new System.Drawing.Size(185, 28);
            this.nationality.TabIndex = 92;
            // 
            // sex
            // 
            this.sex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.sex.Location = new System.Drawing.Point(202, 294);
            this.sex.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.sex.Name = "sex";
            this.sex.Size = new System.Drawing.Size(168, 28);
            this.sex.TabIndex = 89;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(408, 266);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 22);
            this.label7.TabIndex = 81;
            this.label7.Text = "&Nationality";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(198, 266);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 22);
            this.label5.TabIndex = 82;
            this.label5.Text = "&Sex";
            // 
            // given_names
            // 
            this.given_names.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.given_names.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.given_names.Location = new System.Drawing.Point(202, 217);
            this.given_names.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.given_names.Name = "given_names";
            this.given_names.Size = new System.Drawing.Size(395, 28);
            this.given_names.TabIndex = 88;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(198, 189);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 22);
            this.label4.TabIndex = 79;
            this.label4.Text = "&Given Names";
            // 
            // doc_no
            // 
            this.doc_no.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.doc_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.doc_no.Location = new System.Drawing.Point(0, 42);
            this.doc_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.doc_no.Name = "doc_no";
            this.doc_no.Size = new System.Drawing.Size(192, 28);
            this.doc_no.TabIndex = 91;
            this.doc_no.TextChanged += new System.EventHandler(this.doc_no_TextChanged);
            // 
            // family_name
            // 
            this.family_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.family_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.family_name.Location = new System.Drawing.Point(202, 135);
            this.family_name.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.family_name.Name = "family_name";
            this.family_name.Size = new System.Drawing.Size(266, 28);
            this.family_name.TabIndex = 90;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(200, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 22);
            this.label3.TabIndex = 78;
            this.label3.Text = "&Family Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-4, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 22);
            this.label2.TabIndex = 77;
            this.label2.Text = "&Document Number";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // richTextBoxCodeline
            // 
            this.richTextBoxCodeline.BackColor = System.Drawing.Color.White;
            this.richTextBoxCodeline.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxCodeline.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.richTextBoxCodeline.Location = new System.Drawing.Point(6, 538);
            this.richTextBoxCodeline.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.richTextBoxCodeline.Name = "richTextBoxCodeline";
            this.richTextBoxCodeline.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBoxCodeline.Size = new System.Drawing.Size(592, 123);
            this.richTextBoxCodeline.TabIndex = 76;
            this.richTextBoxCodeline.Text = "";
            // 
            // label61
            // 
            this.label61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label61.Location = new System.Drawing.Point(609, 0);
            this.label61.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(3, 666);
            this.label61.TabIndex = 93;
            // 
            // showVerification
            // 
            this.showVerification.BackColor = System.Drawing.Color.White;
            this.showVerification.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showVerification.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.showVerification.ForeColor = System.Drawing.Color.White;
            this.showVerification.Location = new System.Drawing.Point(621, 14);
            this.showVerification.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.showVerification.Name = "showVerification";
            this.showVerification.Size = new System.Drawing.Size(280, 98);
            this.showVerification.TabIndex = 94;
            this.showVerification.Text = "PASSED";
            this.showVerification.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label1.Location = new System.Drawing.Point(621, 135);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 36);
            this.label1.TabIndex = 95;
            this.label1.Text = "Validation";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label13.Location = new System.Drawing.Point(614, 249);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(8);
            this.label13.Size = new System.Drawing.Size(134, 41);
            this.label13.TabIndex = 96;
            this.label13.Text = "Date of Birth";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label15.Location = new System.Drawing.Point(614, 298);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(8);
            this.label15.Size = new System.Drawing.Size(149, 41);
            this.label15.TabIndex = 96;
            this.label15.Text = "Date of Expiry";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label17.Location = new System.Drawing.Point(614, 388);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(8);
            this.label17.Size = new System.Drawing.Size(166, 41);
            this.label17.TabIndex = 96;
            this.label17.Text = "Valid Document";
            // 
            // dob_flag
            // 
            this.dob_flag.AutoSize = true;
            this.dob_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.dob_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dob_flag.ForeColor = System.Drawing.Color.White;
            this.dob_flag.Location = new System.Drawing.Point(806, 252);
            this.dob_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dob_flag.Name = "dob_flag";
            this.dob_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dob_flag.Size = new System.Drawing.Size(86, 35);
            this.dob_flag.TabIndex = 97;
            this.dob_flag.Text = "BLANK";
            // 
            // doe_flag
            // 
            this.doe_flag.AutoSize = true;
            this.doe_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.doe_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.doe_flag.ForeColor = System.Drawing.Color.White;
            this.doe_flag.Location = new System.Drawing.Point(806, 302);
            this.doe_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.doe_flag.Name = "doe_flag";
            this.doe_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.doe_flag.Size = new System.Drawing.Size(86, 35);
            this.doe_flag.TabIndex = 97;
            this.doe_flag.Text = "BLANK";
            // 
            // vd_flag
            // 
            this.vd_flag.AutoSize = true;
            this.vd_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.vd_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.vd_flag.ForeColor = System.Drawing.Color.White;
            this.vd_flag.Location = new System.Drawing.Point(806, 392);
            this.vd_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.vd_flag.Name = "vd_flag";
            this.vd_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.vd_flag.Size = new System.Drawing.Size(86, 35);
            this.vd_flag.TabIndex = 97;
            this.vd_flag.Text = "BLANK";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label26.Location = new System.Drawing.Point(614, 432);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Padding = new System.Windows.Forms.Padding(8);
            this.label26.Size = new System.Drawing.Size(183, 41);
            this.label26.TabIndex = 96;
            this.label26.Text = "Global Checksum";
            // 
            // checks_flag
            // 
            this.checks_flag.AutoSize = true;
            this.checks_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.checks_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.checks_flag.ForeColor = System.Drawing.Color.White;
            this.checks_flag.Location = new System.Drawing.Point(806, 437);
            this.checks_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.checks_flag.Name = "checks_flag";
            this.checks_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checks_flag.Size = new System.Drawing.Size(86, 35);
            this.checks_flag.TabIndex = 97;
            this.checks_flag.Text = "BLANK";
            // 
            // dn
            // 
            this.dn.AutoSize = true;
            this.dn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dn.Location = new System.Drawing.Point(614, 205);
            this.dn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dn.Name = "dn";
            this.dn.Padding = new System.Windows.Forms.Padding(8);
            this.dn.Size = new System.Drawing.Size(147, 41);
            this.dn.TabIndex = 96;
            this.dn.Text = "Document No";
            // 
            // doc_no_flag
            // 
            this.doc_no_flag.AutoSize = true;
            this.doc_no_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.doc_no_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.doc_no_flag.ForeColor = System.Drawing.Color.White;
            this.doc_no_flag.Location = new System.Drawing.Point(806, 206);
            this.doc_no_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.doc_no_flag.Name = "doc_no_flag";
            this.doc_no_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.doc_no_flag.Size = new System.Drawing.Size(86, 35);
            this.doc_no_flag.TabIndex = 97;
            this.doc_no_flag.Text = "BLANK";
            // 
            // expired_txt
            // 
            this.expired_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.expired_txt.Location = new System.Drawing.Point(0, 497);
            this.expired_txt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.expired_txt.Name = "expired_txt";
            this.expired_txt.Size = new System.Drawing.Size(598, 37);
            this.expired_txt.TabIndex = 98;
            this.expired_txt.Click += new System.EventHandler(this.label18_Click);
            // 
            // mrzZoom
            // 
            this.mrzZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mrzZoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mrzZoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.mrzZoom.ForeColor = System.Drawing.Color.White;
            this.mrzZoom.Location = new System.Drawing.Point(2, 302);
            this.mrzZoom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mrzZoom.Name = "mrzZoom";
            this.mrzZoom.Size = new System.Drawing.Size(194, 66);
            this.mrzZoom.TabIndex = 99;
            this.mrzZoom.Text = "View Zoomed";
            this.mrzZoom.UseVisualStyleBackColor = false;
            this.mrzZoom.Click += new System.EventHandler(this.mrzZoom_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label18.Location = new System.Drawing.Point(614, 566);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(8);
            this.label18.Size = new System.Drawing.Size(99, 41);
            this.label18.TabIndex = 96;
            this.label18.Text = "Flagged";
            // 
            // flagged_flag
            // 
            this.flagged_flag.AutoSize = true;
            this.flagged_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.flagged_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.flagged_flag.ForeColor = System.Drawing.Color.White;
            this.flagged_flag.Location = new System.Drawing.Point(806, 571);
            this.flagged_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.flagged_flag.Name = "flagged_flag";
            this.flagged_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flagged_flag.Size = new System.Drawing.Size(86, 35);
            this.flagged_flag.TabIndex = 97;
            this.flagged_flag.Text = "BLANK";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.Location = new System.Drawing.Point(614, 345);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(8);
            this.label10.Size = new System.Drawing.Size(125, 41);
            this.label10.TabIndex = 96;
            this.label10.Text = "Optional ID";
            // 
            // oid_flag
            // 
            this.oid_flag.AutoSize = true;
            this.oid_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.oid_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.oid_flag.ForeColor = System.Drawing.Color.White;
            this.oid_flag.Location = new System.Drawing.Point(806, 349);
            this.oid_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.oid_flag.Name = "oid_flag";
            this.oid_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.oid_flag.Size = new System.Drawing.Size(86, 35);
            this.oid_flag.TabIndex = 97;
            this.oid_flag.Text = "BLANK";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.Location = new System.Drawing.Point(614, 477);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(8);
            this.label12.Size = new System.Drawing.Size(183, 41);
            this.label12.TabIndex = 96;
            this.label12.Text = "P Codeline Match";
            // 
            // cdm_flag
            // 
            this.cdm_flag.AutoSize = true;
            this.cdm_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.cdm_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cdm_flag.ForeColor = System.Drawing.Color.White;
            this.cdm_flag.Location = new System.Drawing.Point(806, 482);
            this.cdm_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cdm_flag.Name = "cdm_flag";
            this.cdm_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cdm_flag.Size = new System.Drawing.Size(86, 35);
            this.cdm_flag.TabIndex = 97;
            this.cdm_flag.Text = "BLANK";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label14.Location = new System.Drawing.Point(614, 522);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(8);
            this.label14.Size = new System.Drawing.Size(166, 41);
            this.label14.TabIndex = 96;
            this.label14.Text = "RFID availability";
            // 
            // rfid_flag
            // 
            this.rfid_flag.AutoSize = true;
            this.rfid_flag.BackColor = System.Drawing.Color.PaleVioletRed;
            this.rfid_flag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rfid_flag.ForeColor = System.Drawing.Color.White;
            this.rfid_flag.Location = new System.Drawing.Point(806, 528);
            this.rfid_flag.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.rfid_flag.Name = "rfid_flag";
            this.rfid_flag.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rfid_flag.Size = new System.Drawing.Size(86, 35);
            this.rfid_flag.TabIndex = 97;
            this.rfid_flag.Text = "BLANK";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(198, 14);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 22);
            this.label16.TabIndex = 77;
            this.label16.Text = "&Optional Data";
            this.label16.Click += new System.EventHandler(this.label2_Click);
            // 
            // opt_data
            // 
            this.opt_data.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.opt_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.opt_data.Location = new System.Drawing.Point(202, 42);
            this.opt_data.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.opt_data.Name = "opt_data";
            this.opt_data.Size = new System.Drawing.Size(395, 28);
            this.opt_data.TabIndex = 91;
            this.opt_data.TextChanged += new System.EventHandler(this.doc_no_TextChanged);
            // 
            // mrzImage
            // 
            this.mrzImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mrzImage.Location = new System.Drawing.Point(3, 111);
            this.mrzImage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mrzImage.Name = "mrzImage";
            this.mrzImage.Size = new System.Drawing.Size(190, 191);
            this.mrzImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mrzImage.TabIndex = 75;
            this.mrzImage.TabStop = false;
            this.mrzImage.Click += new System.EventHandler(this.mrzImage_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(408, 349);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 22);
            this.label11.TabIndex = 81;
            this.label11.Text = "&Age (in years)";
            // 
            // age
            // 
            this.age.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.age.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.age.Location = new System.Drawing.Point(412, 377);
            this.age.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.age.Name = "age";
            this.age.Size = new System.Drawing.Size(185, 28);
            this.age.TabIndex = 92;
            // 
            // MRZScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.mrzZoom);
            this.Controls.Add(this.expired_txt);
            this.Controls.Add(this.flagged_flag);
            this.Controls.Add(this.rfid_flag);
            this.Controls.Add(this.cdm_flag);
            this.Controls.Add(this.checks_flag);
            this.Controls.Add(this.vd_flag);
            this.Controls.Add(this.oid_flag);
            this.Controls.Add(this.doe_flag);
            this.Controls.Add(this.dob_flag);
            this.Controls.Add(this.doc_no_flag);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.dn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.showVerification);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.issuer);
            this.Controls.Add(this.doe);
            this.Controls.Add(this.dob);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.age);
            this.Controls.Add(this.nationality);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.sex);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.given_names);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.opt_data);
            this.Controls.Add(this.doc_no);
            this.Controls.Add(this.family_name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextBoxCodeline);
            this.Controls.Add(this.mrzImage);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MRZScan";
            this.Size = new System.Drawing.Size(927, 666);
            ((System.ComponentModel.ISupportInitialize)(this.mrzImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox issuer;
        public System.Windows.Forms.TextBox doe;
        public System.Windows.Forms.TextBox dob;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox nationality;
        public System.Windows.Forms.TextBox sex;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox given_names;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox doc_no;
        public System.Windows.Forms.TextBox family_name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.RichTextBox richTextBoxCodeline;
        public System.Windows.Forms.PictureBox mrzImage;
        private System.Windows.Forms.Label label61;
        public System.Windows.Forms.Button showVerification;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label dob_flag;
        public System.Windows.Forms.Label doe_flag;
        public System.Windows.Forms.Label vd_flag;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label checks_flag;
        public System.Windows.Forms.Label dn;
        public System.Windows.Forms.Label doc_no_flag;
        public System.Windows.Forms.Label expired_txt;
        private System.Windows.Forms.Button mrzZoom;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label flagged_flag;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label oid_flag;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label cdm_flag;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label rfid_flag;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox opt_data;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox age;
    }
}
