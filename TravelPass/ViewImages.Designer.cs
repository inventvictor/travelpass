﻿namespace TravelPass
{
    partial class ViewImages
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.visImage = new System.Windows.Forms.PictureBox();
            this.visZoom = new System.Windows.Forms.Button();
            this.irImage = new System.Windows.Forms.PictureBox();
            this.irZoom = new System.Windows.Forms.Button();
            this.uvImage = new System.Windows.Forms.PictureBox();
            this.uvZoom = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uvVerify = new System.Windows.Forms.Button();
            this.irVerify = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.visImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.irImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uvImage)).BeginInit();
            this.SuspendLayout();
            // 
            // visImage
            // 
            this.visImage.Location = new System.Drawing.Point(33, 14);
            this.visImage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.visImage.Name = "visImage";
            this.visImage.Size = new System.Drawing.Size(267, 235);
            this.visImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.visImage.TabIndex = 0;
            this.visImage.TabStop = false;
            this.visImage.Click += new System.EventHandler(this.visImage_Click);
            // 
            // visZoom
            // 
            this.visZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.visZoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.visZoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.visZoom.ForeColor = System.Drawing.Color.White;
            this.visZoom.Location = new System.Drawing.Point(33, 249);
            this.visZoom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.visZoom.Name = "visZoom";
            this.visZoom.Size = new System.Drawing.Size(267, 66);
            this.visZoom.TabIndex = 95;
            this.visZoom.Text = "View Zoomed";
            this.visZoom.UseVisualStyleBackColor = false;
            this.visZoom.Click += new System.EventHandler(this.visZoom_Click);
            // 
            // irImage
            // 
            this.irImage.Location = new System.Drawing.Point(482, 14);
            this.irImage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.irImage.Name = "irImage";
            this.irImage.Size = new System.Drawing.Size(267, 235);
            this.irImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.irImage.TabIndex = 0;
            this.irImage.TabStop = false;
            this.irImage.Click += new System.EventHandler(this.irImage_Click);
            // 
            // irZoom
            // 
            this.irZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.irZoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.irZoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.irZoom.ForeColor = System.Drawing.Color.White;
            this.irZoom.Location = new System.Drawing.Point(482, 249);
            this.irZoom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.irZoom.Name = "irZoom";
            this.irZoom.Size = new System.Drawing.Size(267, 66);
            this.irZoom.TabIndex = 95;
            this.irZoom.Text = "View Zoomed";
            this.irZoom.UseVisualStyleBackColor = false;
            this.irZoom.Click += new System.EventHandler(this.irZoom_Click);
            // 
            // uvImage
            // 
            this.uvImage.Location = new System.Drawing.Point(33, 348);
            this.uvImage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uvImage.Name = "uvImage";
            this.uvImage.Size = new System.Drawing.Size(267, 235);
            this.uvImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.uvImage.TabIndex = 0;
            this.uvImage.TabStop = false;
            this.uvImage.Click += new System.EventHandler(this.uvImage_Click);
            // 
            // uvZoom
            // 
            this.uvZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uvZoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uvZoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.uvZoom.ForeColor = System.Drawing.Color.White;
            this.uvZoom.Location = new System.Drawing.Point(33, 583);
            this.uvZoom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uvZoom.Name = "uvZoom";
            this.uvZoom.Size = new System.Drawing.Size(267, 66);
            this.uvZoom.TabIndex = 95;
            this.uvZoom.Text = "View Zoomed";
            this.uvZoom.UseVisualStyleBackColor = false;
            this.uvZoom.Click += new System.EventHandler(this.uvZoom_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(309, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 88);
            this.label1.TabIndex = 96;
            this.label1.Text = "Visual Image Scan\r\n\r\nSize: 178X153\r\nZoom: 983X610";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(758, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 88);
            this.label2.TabIndex = 96;
            this.label2.Text = "IR Image Scan\r\n\r\nSize: 178X153\r\nZoom: 983 X 610";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(309, 348);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 88);
            this.label3.TabIndex = 96;
            this.label3.Text = "UV Image Scan\r\n\r\nSize: 178X153\r\nZoom: 983X610";
            // 
            // uvVerify
            // 
            this.uvVerify.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(0)))));
            this.uvVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uvVerify.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uvVerify.ForeColor = System.Drawing.Color.White;
            this.uvVerify.Location = new System.Drawing.Point(310, 468);
            this.uvVerify.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uvVerify.Name = "uvVerify";
            this.uvVerify.Size = new System.Drawing.Size(146, 71);
            this.uvVerify.TabIndex = 97;
            this.uvVerify.Text = "PASSED";
            this.uvVerify.UseVisualStyleBackColor = false;
            this.uvVerify.Visible = false;
            // 
            // irVerify
            // 
            this.irVerify.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(0)))));
            this.irVerify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.irVerify.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.irVerify.ForeColor = System.Drawing.Color.White;
            this.irVerify.Location = new System.Drawing.Point(759, 135);
            this.irVerify.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.irVerify.Name = "irVerify";
            this.irVerify.Size = new System.Drawing.Size(146, 71);
            this.irVerify.TabIndex = 97;
            this.irVerify.Text = "PASSED";
            this.irVerify.UseVisualStyleBackColor = false;
            this.irVerify.Visible = false;
            // 
            // ViewImages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.irVerify);
            this.Controls.Add(this.uvVerify);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.uvZoom);
            this.Controls.Add(this.irZoom);
            this.Controls.Add(this.visZoom);
            this.Controls.Add(this.uvImage);
            this.Controls.Add(this.irImage);
            this.Controls.Add(this.visImage);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ViewImages";
            this.Size = new System.Drawing.Size(927, 666);
            ((System.ComponentModel.ISupportInitialize)(this.visImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.irImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uvImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox visImage;
        private System.Windows.Forms.Button visZoom;
        public System.Windows.Forms.PictureBox irImage;
        private System.Windows.Forms.Button irZoom;
        public System.Windows.Forms.PictureBox uvImage;
        private System.Windows.Forms.Button uvZoom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button uvVerify;
        public System.Windows.Forms.Button irVerify;
    }
}
