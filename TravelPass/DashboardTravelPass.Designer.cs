﻿namespace TravelPass
{
    partial class DashboardTravelPass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DashboardTravelPass));
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cont_flight = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.new_flight = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pers_name = new System.Windows.Forms.Label();
            this.pers_id = new System.Windows.Forms.Label();
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.syncButton = new System.Windows.Forms.Button();
            this.connectFlag = new System.Windows.Forms.Button();
            this.view_flights = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label6 = new System.Windows.Forms.Label();
            this.create_user = new Bunifu.Framework.UI.BunifuFlatButton();
            this.sign_out = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCards1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.bunifuCards2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.bunifuCards3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.bunifuCards4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 30;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuCards1.Controls.Add(this.pictureBox3);
            this.bunifuCards1.LeftSahddow = true;
            this.bunifuCards1.Location = new System.Drawing.Point(468, 166);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(195, 235);
            this.bunifuCards1.TabIndex = 13;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::TravelPass.Properties.Resources.record;
            this.pictureBox3.Location = new System.Drawing.Point(32, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(133, 140);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.White;
            this.bunifuCards2.BorderRadius = 30;
            this.bunifuCards2.BottomSahddow = true;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuCards2.Controls.Add(this.pictureBox1);
            this.bunifuCards2.Controls.Add(this.cont_flight);
            this.bunifuCards2.LeftSahddow = true;
            this.bunifuCards2.Location = new System.Drawing.Point(242, 165);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = true;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(199, 236);
            this.bunifuCards2.TabIndex = 13;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::TravelPass.Properties.Resources.globe;
            this.pictureBox1.Location = new System.Drawing.Point(30, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // cont_flight
            // 
            this.cont_flight.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.cont_flight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cont_flight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cont_flight.BorderRadius = 8;
            this.cont_flight.ButtonText = "Continue Flight";
            this.cont_flight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cont_flight.Iconcolor = System.Drawing.Color.Transparent;
            this.cont_flight.Iconimage = ((System.Drawing.Image)(resources.GetObject("cont_flight.Iconimage")));
            this.cont_flight.Iconimage_right = null;
            this.cont_flight.Iconimage_right_Selected = null;
            this.cont_flight.Iconimage_Selected = null;
            this.cont_flight.IconZoom = 90D;
            this.cont_flight.IsTab = false;
            this.cont_flight.Location = new System.Drawing.Point(26, 177);
            this.cont_flight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cont_flight.Name = "cont_flight";
            this.cont_flight.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cont_flight.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.cont_flight.OnHoverTextColor = System.Drawing.Color.White;
            this.cont_flight.selected = false;
            this.cont_flight.Size = new System.Drawing.Size(148, 48);
            this.cont_flight.TabIndex = 12;
            this.cont_flight.Textcolor = System.Drawing.Color.White;
            this.cont_flight.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cont_flight.Click += new System.EventHandler(this.cont_flight_Click);
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.White;
            this.bunifuCards3.BorderRadius = 30;
            this.bunifuCards3.BottomSahddow = true;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuCards3.Controls.Add(this.new_flight);
            this.bunifuCards3.Controls.Add(this.pictureBox2);
            this.bunifuCards3.LeftSahddow = true;
            this.bunifuCards3.Location = new System.Drawing.Point(13, 165);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = true;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(199, 236);
            this.bunifuCards3.TabIndex = 13;
            // 
            // new_flight
            // 
            this.new_flight.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.new_flight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.new_flight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.new_flight.BorderRadius = 8;
            this.new_flight.ButtonText = "New Flight";
            this.new_flight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.new_flight.Iconcolor = System.Drawing.Color.Transparent;
            this.new_flight.Iconimage = ((System.Drawing.Image)(resources.GetObject("new_flight.Iconimage")));
            this.new_flight.Iconimage_right = null;
            this.new_flight.Iconimage_right_Selected = null;
            this.new_flight.Iconimage_Selected = null;
            this.new_flight.IconZoom = 90D;
            this.new_flight.IsTab = false;
            this.new_flight.Location = new System.Drawing.Point(23, 178);
            this.new_flight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.new_flight.Name = "new_flight";
            this.new_flight.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.new_flight.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.new_flight.OnHoverTextColor = System.Drawing.Color.White;
            this.new_flight.selected = false;
            this.new_flight.Size = new System.Drawing.Size(148, 48);
            this.new_flight.TabIndex = 12;
            this.new_flight.Textcolor = System.Drawing.Color.White;
            this.new_flight.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_flight.Click += new System.EventHandler(this.new_flight_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::TravelPass.Properties.Resources.airplane;
            this.pictureBox2.Location = new System.Drawing.Point(34, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(127, 140);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(73, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "ALML";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(73, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Dashboard";
            // 
            // pers_name
            // 
            this.pers_name.AutoSize = true;
            this.pers_name.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pers_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pers_name.Location = new System.Drawing.Point(166, 8);
            this.pers_name.Name = "pers_name";
            this.pers_name.Size = new System.Drawing.Size(153, 23);
            this.pers_name.TabIndex = 10;
            this.pers_name.Text = "CHIZOBA ARINZE";
            // 
            // pers_id
            // 
            this.pers_id.AutoSize = true;
            this.pers_id.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pers_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pers_id.Location = new System.Drawing.Point(166, 31);
            this.pers_id.Name = "pers_id";
            this.pers_id.Size = new System.Drawing.Size(59, 19);
            this.pers_id.TabIndex = 10;
            this.pers_id.Text = "USR453";
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.BackColor = System.Drawing.Color.White;
            this.bunifuCards4.BorderRadius = 0;
            this.bunifuCards4.BottomSahddow = true;
            this.bunifuCards4.color = System.Drawing.Color.White;
            this.bunifuCards4.Controls.Add(this.syncButton);
            this.bunifuCards4.Controls.Add(this.connectFlag);
            this.bunifuCards4.Controls.Add(this.view_flights);
            this.bunifuCards4.Controls.Add(this.bunifuCards3);
            this.bunifuCards4.Controls.Add(this.bunifuCards2);
            this.bunifuCards4.Controls.Add(this.bunifuCards1);
            this.bunifuCards4.Controls.Add(this.label6);
            this.bunifuCards4.Controls.Add(this.create_user);
            this.bunifuCards4.Controls.Add(this.sign_out);
            this.bunifuCards4.Controls.Add(this.pers_id);
            this.bunifuCards4.Controls.Add(this.pers_name);
            this.bunifuCards4.Controls.Add(this.bunifuImageButton2);
            this.bunifuCards4.Controls.Add(this.label2);
            this.bunifuCards4.Controls.Add(this.label1);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(-1, 1);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = true;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(676, 528);
            this.bunifuCards4.TabIndex = 14;
            // 
            // syncButton
            // 
            this.syncButton.BackColor = System.Drawing.SystemColors.Window;
            this.syncButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.syncButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.syncButton.ForeColor = System.Drawing.Color.SlateBlue;
            this.syncButton.Location = new System.Drawing.Point(158, 442);
            this.syncButton.Name = "syncButton";
            this.syncButton.Size = new System.Drawing.Size(353, 55);
            this.syncButton.TabIndex = 50;
            this.syncButton.Text = "Sync Local DB with WD";
            this.syncButton.UseVisualStyleBackColor = false;
            this.syncButton.Click += new System.EventHandler(this.syncButton_Click);
            // 
            // connectFlag
            // 
            this.connectFlag.BackColor = System.Drawing.Color.White;
            this.connectFlag.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connectFlag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.connectFlag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(168)))), ((int)(((byte)(0)))));
            this.connectFlag.Location = new System.Drawing.Point(468, 90);
            this.connectFlag.Name = "connectFlag";
            this.connectFlag.Size = new System.Drawing.Size(195, 33);
            this.connectFlag.TabIndex = 50;
            this.connectFlag.Text = "Connected";
            this.connectFlag.UseVisualStyleBackColor = false;
            // 
            // view_flights
            // 
            this.view_flights.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.view_flights.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.view_flights.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view_flights.BorderRadius = 8;
            this.view_flights.ButtonText = "View Flights";
            this.view_flights.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view_flights.Iconcolor = System.Drawing.Color.Transparent;
            this.view_flights.Iconimage = ((System.Drawing.Image)(resources.GetObject("view_flights.Iconimage")));
            this.view_flights.Iconimage_right = null;
            this.view_flights.Iconimage_right_Selected = null;
            this.view_flights.Iconimage_Selected = null;
            this.view_flights.IconZoom = 90D;
            this.view_flights.IsTab = false;
            this.view_flights.Location = new System.Drawing.Point(491, 342);
            this.view_flights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.view_flights.Name = "view_flights";
            this.view_flights.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.view_flights.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.view_flights.OnHoverTextColor = System.Drawing.Color.White;
            this.view_flights.selected = false;
            this.view_flights.Size = new System.Drawing.Size(148, 48);
            this.view_flights.TabIndex = 12;
            this.view_flights.Textcolor = System.Drawing.Color.White;
            this.view_flights.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_flights.Click += new System.EventHandler(this.view_flights_Click);
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(158, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(2, 50);
            this.label6.TabIndex = 17;
            // 
            // create_user
            // 
            this.create_user.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.create_user.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.create_user.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.create_user.BorderRadius = 0;
            this.create_user.ButtonText = "Create User";
            this.create_user.Cursor = System.Windows.Forms.Cursors.Hand;
            this.create_user.Iconcolor = System.Drawing.Color.Transparent;
            this.create_user.Iconimage = ((System.Drawing.Image)(resources.GetObject("create_user.Iconimage")));
            this.create_user.Iconimage_right = null;
            this.create_user.Iconimage_right_Selected = null;
            this.create_user.Iconimage_Selected = null;
            this.create_user.IconZoom = 90D;
            this.create_user.IsTab = false;
            this.create_user.Location = new System.Drawing.Point(141, 90);
            this.create_user.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.create_user.Name = "create_user";
            this.create_user.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.create_user.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.create_user.OnHoverTextColor = System.Drawing.Color.White;
            this.create_user.selected = false;
            this.create_user.Size = new System.Drawing.Size(134, 33);
            this.create_user.TabIndex = 16;
            this.create_user.Textcolor = System.Drawing.Color.White;
            this.create_user.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.create_user.Click += new System.EventHandler(this.create_user_click);
            // 
            // sign_out
            // 
            this.sign_out.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sign_out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sign_out.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sign_out.BorderRadius = 0;
            this.sign_out.ButtonText = "Sign out";
            this.sign_out.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sign_out.Iconcolor = System.Drawing.Color.Transparent;
            this.sign_out.Iconimage = ((System.Drawing.Image)(resources.GetObject("sign_out.Iconimage")));
            this.sign_out.Iconimage_right = null;
            this.sign_out.Iconimage_right_Selected = null;
            this.sign_out.Iconimage_Selected = null;
            this.sign_out.IconZoom = 90D;
            this.sign_out.IsTab = false;
            this.sign_out.Location = new System.Drawing.Point(13, 90);
            this.sign_out.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.sign_out.Name = "sign_out";
            this.sign_out.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sign_out.OnHovercolor = System.Drawing.Color.Red;
            this.sign_out.OnHoverTextColor = System.Drawing.Color.White;
            this.sign_out.selected = false;
            this.sign_out.Size = new System.Drawing.Size(110, 33);
            this.sign_out.TabIndex = 16;
            this.sign_out.Textcolor = System.Drawing.Color.White;
            this.sign_out.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sign_out.Click += new System.EventHandler(this.sign_out_click);
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.BackColor = System.Drawing.Color.White;
            this.bunifuImageButton2.Image = global::TravelPass.Properties.Resources.newpng;
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(13, 8);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(54, 49);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 14;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            // 
            // DashboardTravelPass
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(674, 528);
            this.Controls.Add(this.bunifuCards4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DashboardTravelPass";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.DashboardTravelPass_Load);
            this.bunifuCards1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.bunifuCards2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.bunifuCards3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Bunifu.Framework.UI.BunifuFlatButton new_flight;
        private Bunifu.Framework.UI.BunifuFlatButton cont_flight;
        private Bunifu.Framework.UI.BunifuFlatButton view_flights;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label pers_name;
        private System.Windows.Forms.Label pers_id;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private Bunifu.Framework.UI.BunifuFlatButton create_user;
        private Bunifu.Framework.UI.BunifuFlatButton sign_out;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button connectFlag;
        private System.Windows.Forms.Button syncButton;
    }
}

