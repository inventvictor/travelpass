﻿namespace TravelPass
{
    partial class UpdateRecordList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateRecordList));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.record_combo = new System.Windows.Forms.ComboBox();
            search_btn = new System.Windows.Forms.Button();
            search_box = new System.Windows.Forms.TextBox();
            view_record__ = new System.Windows.Forms.Button();
            final_dest_box_ = new System.Windows.Forms.TextBox();
            flight_to_box_ = new System.Windows.Forms.TextBox();
            flight_from_box_ = new System.Windows.Forms.TextBox();
            datetime_recorded_box_ = new System.Windows.Forms.TextBox();
            recordedby_email_box_ = new System.Windows.Forms.TextBox();
            recordedby_name_box_ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.recordsDataGrid = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recordsDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(view_record__);
            this.groupBox1.Controls.Add(final_dest_box_);
            this.groupBox1.Controls.Add(flight_to_box_);
            this.groupBox1.Controls.Add(flight_from_box_);
            this.groupBox1.Controls.Add(datetime_recorded_box_);
            this.groupBox1.Controls.Add(recordedby_email_box_);
            this.groupBox1.Controls.Add(recordedby_name_box_);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1094, 117);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Record Details";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.record_combo);
            this.groupBox2.Controls.Add(search_btn);
            this.groupBox2.Controls.Add(search_box);
            this.groupBox2.Location = new System.Drawing.Point(822, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 97);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search";
            // 
            // record_combo
            // 
            this.record_combo.FormattingEnabled = true;
            this.record_combo.Items.AddRange(new object[] {
            "Record Folder Path",
            "Recorded_by Name",
            "Recorded_by Email",
            "Scanned Passport Number",
            "Scanned Passport Name",
            "Date Time Recorded",
            "Flight From",
            "Flight To",
            "Final Destination"});
            this.record_combo.Location = new System.Drawing.Point(9, 21);
            this.record_combo.Name = "record_combo";
            this.record_combo.Size = new System.Drawing.Size(160, 23);
            this.record_combo.TabIndex = 2;
            this.record_combo.Text = "Recorded_by Name";
            // 
            // search_btn
            // 
            search_btn.BackColor = System.Drawing.Color.White;
            search_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            search_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            search_btn.Location = new System.Drawing.Point(183, 42);
            search_btn.Name = "search_btn";
            search_btn.Size = new System.Drawing.Size(75, 23);
            search_btn.TabIndex = 26;
            search_btn.Text = "Search";
            search_btn.UseVisualStyleBackColor = false;
            search_btn.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // search_box
            // 
            search_box.Location = new System.Drawing.Point(9, 62);
            search_box.Name = "search_box";
            search_box.Size = new System.Drawing.Size(160, 21);
            search_box.TabIndex = 1;
            // 
            // view_record__
            // 
            view_record__.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(0)))));
            view_record__.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            view_record__.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            view_record__.ForeColor = System.Drawing.Color.White;
            view_record__.Location = new System.Drawing.Point(12, 36);
            view_record__.Name = "view_record__";
            view_record__.Size = new System.Drawing.Size(112, 33);
            view_record__.TabIndex = 26;
            view_record__.Text = "View Record";
            view_record__.UseVisualStyleBackColor = false;
            view_record__.Click += new System.EventHandler(view_record__Click);
            // 
            // final_dest_box_
            // 
            final_dest_box_.Location = new System.Drawing.Point(606, 72);
            final_dest_box_.Name = "final_dest_box_";
            final_dest_box_.Size = new System.Drawing.Size(206, 21);
            final_dest_box_.TabIndex = 1;
            // 
            // flight_to_box_
            // 
            flight_to_box_.Location = new System.Drawing.Point(145, 72);
            flight_to_box_.Name = "flight_to_box_";
            flight_to_box_.Size = new System.Drawing.Size(206, 21);
            flight_to_box_.TabIndex = 1;
            // 
            // flight_from_box_
            // 
            flight_from_box_.Location = new System.Drawing.Point(375, 72);
            flight_from_box_.Name = "flight_from_box_";
            flight_from_box_.Size = new System.Drawing.Size(206, 21);
            flight_from_box_.TabIndex = 1;
            // 
            // datetime_recorded_box_
            // 
            datetime_recorded_box_.Location = new System.Drawing.Point(606, 32);
            datetime_recorded_box_.Name = "datetime_recorded_box_";
            datetime_recorded_box_.Size = new System.Drawing.Size(206, 21);
            datetime_recorded_box_.TabIndex = 1;
            // 
            // recordedby_email_box_
            // 
            recordedby_email_box_.Location = new System.Drawing.Point(145, 32);
            recordedby_email_box_.Name = "recordedby_email_box_";
            recordedby_email_box_.Size = new System.Drawing.Size(206, 21);
            recordedby_email_box_.TabIndex = 1;
            // 
            // recordedby_name_box_
            // 
            recordedby_name_box_.Location = new System.Drawing.Point(374, 32);
            recordedby_name_box_.Name = "recordedby_name_box_";
            recordedby_name_box_.Size = new System.Drawing.Size(206, 21);
            recordedby_name_box_.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.Location = new System.Drawing.Point(602, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Final Destination";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.Location = new System.Drawing.Point(142, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Flight To";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.Location = new System.Drawing.Point(603, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Date-Time Recorded";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.Location = new System.Drawing.Point(142, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Recorded By_Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(371, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Flight From";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(371, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Recorded by_Name";
            // 
            // recordsDataGrid
            // 
            this.recordsDataGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.recordsDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.recordsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.recordsDataGrid.Location = new System.Drawing.Point(12, 115);
            this.recordsDataGrid.Name = "recordsDataGrid";
            this.recordsDataGrid.RowHeadersWidth = 200;
            this.recordsDataGrid.Size = new System.Drawing.Size(1071, 431);
            this.recordsDataGrid.TabIndex = 2;
            this.recordsDataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.recordsDataGrid_CellClick);
            // 
            // UpdateRecordList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1094, 558);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.recordsDataGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateRecordList";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Records List";
            this.Load += new System.EventHandler(this.UpdateRecordList_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recordsDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private static System.Windows.Forms.Button view_record;
        private static System.Windows.Forms.TextBox final_dest_box;
        private static System.Windows.Forms.TextBox flight_to_box;
        private static System.Windows.Forms.TextBox flight_from_box;
        private static System.Windows.Forms.TextBox datetime_recorded_box;
        private System.Windows.Forms.Label label6;
        private static System.Windows.Forms.TextBox recordedby_email_box;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private static System.Windows.Forms.TextBox recordedby_name_box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView recordsDataGrid;
        private static System.Windows.Forms.Button view_record_;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox record_combo;
        private static System.Windows.Forms.Button search_btn;
        private static System.Windows.Forms.TextBox search_box;
        private static System.Windows.Forms.Button view_record__;
        private static System.Windows.Forms.TextBox final_dest_box_;
        private static System.Windows.Forms.TextBox flight_to_box_;
        private static System.Windows.Forms.TextBox flight_from_box_;
        private static System.Windows.Forms.TextBox datetime_recorded_box_;
        private static System.Windows.Forms.TextBox recordedby_email_box_;
        private static System.Windows.Forms.TextBox recordedby_name_box_;
    }
}









