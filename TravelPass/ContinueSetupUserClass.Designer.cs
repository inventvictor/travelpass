﻿namespace TravelPass
{
    partial class ContinueSetupUserClass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.quitSetupLabel = new System.Windows.Forms.Label();
            this.done = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.state = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.phone_textbox = new System.Windows.Forms.TextBox();
            this.passwd_textbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.user_class = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.user_class_combo = new System.Windows.Forms.ComboBox();
            this.state_textbox = new System.Windows.Forms.TextBox();
            this.quitSetup = new System.Windows.Forms.PictureBox();
            this.confirm_passwd_textbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.quitSetup)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Setup User Class";
            // 
            // quitSetupLabel
            // 
            this.quitSetupLabel.AutoSize = true;
            this.quitSetupLabel.Location = new System.Drawing.Point(259, 33);
            this.quitSetupLabel.Name = "quitSetupLabel";
            this.quitSetupLabel.Size = new System.Drawing.Size(26, 13);
            this.quitSetupLabel.TabIndex = 23;
            this.quitSetupLabel.Text = "Quit";
            this.quitSetupLabel.Visible = false;
            // 
            // done
            // 
            this.done.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.done.Font = new System.Drawing.Font("Microsoft JhengHei UI Light", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.done.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.done.Location = new System.Drawing.Point(159, 371);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(103, 28);
            this.done.TabIndex = 20;
            this.done.Text = "Done";
            this.done.UseVisualStyleBackColor = false;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.back.Font = new System.Drawing.Font("Microsoft JhengHei UI Light", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.ForeColor = System.Drawing.Color.White;
            this.back.Location = new System.Drawing.Point(3, 371);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(103, 28);
            this.back.TabIndex = 19;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 15);
            this.label6.TabIndex = 18;
            // 
            // state
            // 
            this.state.AutoSize = true;
            this.state.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.state.Location = new System.Drawing.Point(5, 238);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(93, 15);
            this.state.TabIndex = 16;
            this.state.Text = "State (Location)";
            // 
            // phone
            // 
            this.phone.AutoSize = true;
            this.phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone.Location = new System.Drawing.Point(5, 186);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(43, 15);
            this.phone.TabIndex = 15;
            this.phone.Text = "Phone";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(7, 84);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(61, 15);
            this.password.TabIndex = 14;
            this.password.Text = "Password";
            // 
            // phone_textbox
            // 
            this.phone_textbox.AutoCompleteCustomSource.AddRange(new string[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua & Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia & Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Myanmar/Burma",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombia",
            "Comoros",
            "Congo",
            "Costa Rica",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Democratic Republic of the Congo",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Fiji",
            "Finland",
            "France",
            "French Guiana",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Great Britain",
            "Greece",
            "Grenada",
            "Guadeloupe",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Israel and the Occupied Territories",
            "Italy",
            "Ivory Coast (Cote d\'Ivoire)",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kosovo",
            "Kuwait",
            "Kyrgyz Republic (Kyrgyzstan)",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Republic of Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Moldova, Republic of",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Nepal",
            "Netherlands",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Korea, Democratic Republic of (North Korea)",
            "Norway",
            "Oman",
            "Pacific Islands",
            "Pakistan",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent\'s & Grenadines",
            "Samoa",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovak Republic (Slovakia)",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Korea, Republic of (South Korea)",
            "South Sudan",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Timor Leste",
            "Togo",
            "Trinidad & Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks & Caicos Islands",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United States of America (USA)",
            "Uruguay",
            "Uzbekistan",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (UK)",
            "Virgin Islands (US)",
            "Yemen",
            "Zambia",
            "Zimbabwe"});
            this.phone_textbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.phone_textbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.phone_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone_textbox.ForeColor = System.Drawing.SystemColors.GrayText;
            this.phone_textbox.Location = new System.Drawing.Point(5, 204);
            this.phone_textbox.Name = "phone_textbox";
            this.phone_textbox.Size = new System.Drawing.Size(259, 21);
            this.phone_textbox.TabIndex = 9;
            this.phone_textbox.Text = "e.g +2348185476560";
            this.phone_textbox.TextChanged += new System.EventHandler(this.phone_textbox_TextChanged);
            this.phone_textbox.Enter += new System.EventHandler(this.phone_textbox_Enter);
            this.phone_textbox.Leave += new System.EventHandler(this.phone_textbox_Leave);
            // 
            // passwd_textbox
            // 
            this.passwd_textbox.AutoCompleteCustomSource.AddRange(new string[] {
            "Arik Air",
            "Air Nigeria",
            "Allied Air",
            "IRS Airlines",
            "Pan African Airlines",
            "Air Afrique",
            "Air France",
            "KLM",
            "Air India",
            "Atlantic Express",
            "Alitalia",
            "British Airways",
            "Bellview Airlines",
            "China Southern Airlines",
            "Delta Airlines",
            "Emirates",
            "Egypt Air",
            "Ethiopian Airlines",
            "Ghana Airways",
            "Kenya Airways",
            "Lufthansa German Airlines",
            "Middle East Airlines",
            "Qatar Airways",
            "Saudi Air",
            "South African Airlines",
            "Turkish Airlines",
            "Virgin Atlantic",
            "Virgin Nigeria"});
            this.passwd_textbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.passwd_textbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.passwd_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwd_textbox.ForeColor = System.Drawing.SystemColors.GrayText;
            this.passwd_textbox.Location = new System.Drawing.Point(7, 101);
            this.passwd_textbox.Name = "passwd_textbox";
            this.passwd_textbox.PasswordChar = '*';
            this.passwd_textbox.Size = new System.Drawing.Size(259, 21);
            this.passwd_textbox.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "TravelPass";
            // 
            // user_class
            // 
            this.user_class.AutoSize = true;
            this.user_class.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_class.Location = new System.Drawing.Point(6, 291);
            this.user_class.Name = "user_class";
            this.user_class.Size = new System.Drawing.Size(64, 15);
            this.user_class.TabIndex = 16;
            this.user_class.Text = "User class";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(138)))), ((int)(((byte)(214)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Location = new System.Drawing.Point(162, 421);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 28);
            this.button1.TabIndex = 20;
            this.button1.Text = "Done";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // user_class_combo
            // 
            this.user_class_combo.AutoCompleteCustomSource.AddRange(new string[] {
            "Admin",
            "Supervisor",
            "User"});
            this.user_class_combo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.user_class_combo.ForeColor = System.Drawing.SystemColors.GrayText;
            this.user_class_combo.FormattingEnabled = true;
            this.user_class_combo.Items.AddRange(new object[] {
            "Admin",
            "Supervisor",
            "User"});
            this.user_class_combo.Location = new System.Drawing.Point(6, 309);
            this.user_class_combo.Name = "user_class_combo";
            this.user_class_combo.Size = new System.Drawing.Size(259, 21);
            this.user_class_combo.TabIndex = 26;
            this.user_class_combo.Text = "Please set personnel role";
            this.user_class_combo.Enter += new System.EventHandler(this.user_class_combo_Enter);
            this.user_class_combo.Leave += new System.EventHandler(this.user_class_combo_Leave);
            // 
            // state_textbox
            // 
            this.state_textbox.AutoCompleteCustomSource.AddRange(new string[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua & Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia & Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Myanmar/Burma",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombia",
            "Comoros",
            "Congo",
            "Costa Rica",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Democratic Republic of the Congo",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Fiji",
            "Finland",
            "France",
            "French Guiana",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Great Britain",
            "Greece",
            "Grenada",
            "Guadeloupe",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Israel and the Occupied Territories",
            "Italy",
            "Ivory Coast (Cote d\'Ivoire)",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kosovo",
            "Kuwait",
            "Kyrgyz Republic (Kyrgyzstan)",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Republic of Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Moldova, Republic of",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Nepal",
            "Netherlands",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Korea, Democratic Republic of (North Korea)",
            "Norway",
            "Oman",
            "Pacific Islands",
            "Pakistan",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent\'s & Grenadines",
            "Samoa",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovak Republic (Slovakia)",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Korea, Republic of (South Korea)",
            "South Sudan",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Timor Leste",
            "Togo",
            "Trinidad & Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks & Caicos Islands",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United States of America (USA)",
            "Uruguay",
            "Uzbekistan",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (UK)",
            "Virgin Islands (US)",
            "Yemen",
            "Zambia",
            "Zimbabwe"});
            this.state_textbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.state_textbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.state_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.state_textbox.ForeColor = System.Drawing.SystemColors.GrayText;
            this.state_textbox.Location = new System.Drawing.Point(6, 256);
            this.state_textbox.Name = "state_textbox";
            this.state_textbox.Size = new System.Drawing.Size(259, 21);
            this.state_textbox.TabIndex = 27;
            this.state_textbox.Text = "e.g Lagos";
            this.state_textbox.TextChanged += new System.EventHandler(this.state_textbox_TextChanged);
            this.state_textbox.Enter += new System.EventHandler(this.state_textbox_Enter);
            this.state_textbox.Leave += new System.EventHandler(this.state_textbox_Leave);
            // 
            // quitSetup
            // 
            this.quitSetup.Image = global::TravelPass.Properties.Resources.Delete_96px;
            this.quitSetup.Location = new System.Drawing.Point(262, 3);
            this.quitSetup.Name = "quitSetup";
            this.quitSetup.Size = new System.Drawing.Size(28, 27);
            this.quitSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.quitSetup.TabIndex = 21;
            this.quitSetup.TabStop = false;
            this.quitSetup.Click += new System.EventHandler(this.quitSetup_Click);
            this.quitSetup.MouseEnter += new System.EventHandler(this.quitSetup_MouseEnter);
            this.quitSetup.MouseLeave += new System.EventHandler(this.quitSetup_MouseLeave);
            this.quitSetup.MouseHover += new System.EventHandler(this.quitSetup_MouseHover);
            // 
            // confirm_passwd_textbox
            // 
            this.confirm_passwd_textbox.AutoCompleteCustomSource.AddRange(new string[] {
            "Arik Air",
            "Air Nigeria",
            "Allied Air",
            "IRS Airlines",
            "Pan African Airlines",
            "Air Afrique",
            "Air France",
            "KLM",
            "Air India",
            "Atlantic Express",
            "Alitalia",
            "British Airways",
            "Bellview Airlines",
            "China Southern Airlines",
            "Delta Airlines",
            "Emirates",
            "Egypt Air",
            "Ethiopian Airlines",
            "Ghana Airways",
            "Kenya Airways",
            "Lufthansa German Airlines",
            "Middle East Airlines",
            "Qatar Airways",
            "Saudi Air",
            "South African Airlines",
            "Turkish Airlines",
            "Virgin Atlantic",
            "Virgin Nigeria"});
            this.confirm_passwd_textbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.confirm_passwd_textbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.confirm_passwd_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirm_passwd_textbox.ForeColor = System.Drawing.SystemColors.GrayText;
            this.confirm_passwd_textbox.Location = new System.Drawing.Point(5, 153);
            this.confirm_passwd_textbox.Name = "confirm_passwd_textbox";
            this.confirm_passwd_textbox.PasswordChar = '*';
            this.confirm_passwd_textbox.Size = new System.Drawing.Size(259, 21);
            this.confirm_passwd_textbox.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 14;
            this.label2.Text = "Confirm Password";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(74, 86);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(34, 13);
            this.linkLabel1.TabIndex = 28;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Show";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(118, 138);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(34, 13);
            this.linkLabel2.TabIndex = 28;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Show";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // ContinueSetupUserClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.state_textbox);
            this.Controls.Add(this.user_class_combo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.quitSetupLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.quitSetup);
            this.Controls.Add(this.done);
            this.Controls.Add(this.back);
            this.Controls.Add(this.user_class);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.state);
            this.Controls.Add(this.phone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.password);
            this.Controls.Add(this.phone_textbox);
            this.Controls.Add(this.confirm_passwd_textbox);
            this.Controls.Add(this.passwd_textbox);
            this.Controls.Add(this.label1);
            this.Name = "ContinueSetupUserClass";
            this.Size = new System.Drawing.Size(290, 406);
            ((System.ComponentModel.ISupportInitialize)(this.quitSetup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label quitSetupLabel;
        private System.Windows.Forms.PictureBox quitSetup;
        private System.Windows.Forms.Button done;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label state;
        private System.Windows.Forms.Label phone;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.TextBox phone_textbox;
        private System.Windows.Forms.TextBox passwd_textbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label user_class;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox user_class_combo;
        private System.Windows.Forms.TextBox state_textbox;
        private System.Windows.Forms.TextBox confirm_passwd_textbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
    }
}
